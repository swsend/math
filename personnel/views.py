#coding=utf-8
from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404
from django.template import RequestContext

from Math.personnel.models import Personnel 
# Create your views here.

def personnel(request):
    personnel_list = Personnel.objects.all()	
    return render_to_response("personnel.html",{"personnel_list":personnel_list},RequestContext(request))
