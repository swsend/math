#coding:utf-8
from django.db import models


class Lecture(models.Model):
    date = models.DateTimeField()
    adress = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    lecturer = models.CharField(max_length = 20) 
    is_active = models.BooleanField(default =True)




