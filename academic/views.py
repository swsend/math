#coding=utf-8
from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render, render_to_response, get_object_or_404

from Math.academic.models import Lecture
# Create your views here.


def academic(request):
    lecture_list = Lecture.objects.all()
    return render_to_response("academic.html",{"lecture_list":lecture_list},RequestContext(request))
    
    #return render(request, "academic.html", {"lecture_list": lecture_list})
