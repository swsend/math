# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('academic', '0002_lecture_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
