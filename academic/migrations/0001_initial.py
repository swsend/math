# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lecture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField()),
                ('adress', models.CharField(max_length=100)),
                ('title', models.CharField(max_length=100)),
                ('lecturer', models.CharField(max_length=20)),
            ],
        ),
    ]
