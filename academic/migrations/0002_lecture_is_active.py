# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('academic', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='lecture',
            name='is_active',
            field=models.BooleanField(default=datetime.datetime(2016, 3, 7, 11, 23, 11, 925904, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
