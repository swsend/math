#coding:utf-8
from django.db import models

class Column(models.Model):
	id = models.CharField(primary_key = True,max_length = 10)
	name = models.CharField(max_length = 50)
	order = models.IntegerField()
	is_active = models.BooleanField(default = True)

	class meta:
		ordering = ['order']


class Dpartments(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField()
	is_active = models.BooleanField(default = True)
	column = models.ForeignKey(Column)

	class meta:
		ordering = ['order']


class Personnel(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField()
	is_active = models.BooleanField(default = True)
	column = models.ForeignKey(Column)

	class meta:
		ordering

class Undergraduate(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField(max_length = 2)
	is_active = models.BooleanField()
	column = models.ForeignKey(Column)


class Postgraduate(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField(max_length = 2)
	is_active = models.BooleanField()
	column = models.ForeignKey(Column)

class Academic(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField(max_length = 2)
	is_active = models.BooleanField()
	column = models.ForeignKey(Column)

class Activity(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField(max_length = 2)
	is_active = models.BooleanField()
	column = models.ForeignKey(Column)

class Inside(models.Model):
	name = models.CharField(max_length = 50)
	order = models.IntegerField(max_length = 2)
	is_active = models.BooleanField()
	column = models.ForeignKey(Column)


